import { useEffect, useState } from "react"
import Beer from "../../components/Beer"
import beerApi from "../../services/apis/beer"
import { Container } from "./catalogue.style"

const item = {
	visible: (i) => ({
		opacity: 1,
		y: 0,
		transition: {
			delay: i * 0.1,
		},
	}),
	hidden: { opacity: 0, y: 30 },
}

const container = {
	visible: {
		opacity: 1,
		transition: {
			when: "beforeChildren",
			staggerChildren: 0.3,
		},
	},
	hidden: {
		opacity: 0,
		transition: {
			when: "afterChildren",
		},
	},
}

const Catalogue = () => {
	const [beers, setBeers] = useState([])

	useEffect(() => {
		beerApi
			.get("/")
			.then((resp) => setBeers(resp.data))
			.catch((err) => console.log(err))
	}, [])

	return (
		<Container variants={container} initial="hidden" animate="visible">
			{beers.map((beer, i) => (
				<Beer
					custom={i}
					initial="hidden"
					animate="visible"
					variants={item}
					key={beer.id}
					beer={beer}
					catalogue
				/>
			))}
		</Container>
	)
}

export default Catalogue
