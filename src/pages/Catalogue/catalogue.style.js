import { motion } from "framer-motion"
import styled from "styled-components"

export const Container = styled(motion.div)`
	padding: 2rem 0.5rem;
	display: flex;
	flex-wrap: wrap;
	gap: 0.5rem;
	justify-content: center;
	align-items: center;
	margin: 0 auto;
	max-width: 1200px;

	@media only screen and (min-width: 750px) {
		gap: 1.2rem;
	}
`
