import { useEffect, useState } from "react"
import { useHistory, useParams } from "react-router-dom"
import beerApi from "../../services/apis/beer"
import { Container } from "./beerPage.style"
import beerDecoration from "../../assets/beer_decoration.svg"
import { useWeeding } from "../../Providers/weeding"
import { useGraduation } from "../../Providers/graduation"
import { useGetTogether } from "../../Providers/getTogether"
import { BsArrowReturnLeft } from "react-icons/bs"

const BeerPage = () => {
	const history = useHistory()
	const { id } = useParams()
	const [beer, setBeer] = useState({})
	const {
		image_url: url,
		name,
		tagline,
		description,
		boil_volume,
		first_brewed,
		food_pairing,
	} = beer
	const { addBeer: weedingAdd } = useWeeding()
	const { addBeer: graduationAdd } = useGraduation()
	const { addBeer: getTogetherAdd } = useGetTogether()

	useEffect(() => {
		beerApi
			.get(`/${id}`)
			.then(({ data }) => setBeer(data[0]))
			.catch((err) => console.log(err))
	}, [id])

	return (
		<Container>
			<figure>
				<h2 className="colored_text">
					<span onClick={() => history.goBack()}>
						<BsArrowReturnLeft />
					</span>
					{name}
				</h2>
				<div
					className={
						url?.toLowerCase().endsWith("keg.png")
							? "img_container default_img"
							: "img_container"
					}
				>
					<img src={url} alt={name} />
				</div>
				<p>
					<img src={beerDecoration} alt="decoration" />
				</p>
			</figure>
			<div>
				<p className="beer__tagline">
					<em>{tagline}</em>
				</p>
				<h3>Desc.</h3>
				<p className="beer__description">{description}</p>
				<div className="add_beer">
					<p>add this 🍺 to your</p>
					<div>
						<button onClick={() => weedingAdd(beer)}>
							<em>weeding</em>
						</button>
						<button onClick={() => getTogetherAdd(beer)}>
							<em>get together</em>
						</button>
						<button onClick={() => graduationAdd(beer)}>
							<em>graduation</em>
						</button>
					</div>
				</div>
				<h3>Info.</h3>
				<ul>
					<li dataatrr="🍺">{`${boil_volume?.value} ${boil_volume?.unit}`}</li>
					<li dataatrr="🗓️">First brewed in {first_brewed}</li>
					<li className="food_pairing" dataatrr="🍲">
						Food Pairing:
						<ul>
							{food_pairing?.map((food) => (
								<li key={food}>{food}</li>
							))}
						</ul>
					</li>
				</ul>
			</div>
		</Container>
	)
}

export default BeerPage
