import styled from "styled-components"
import defaultBeer from "../../assets/defaultBeer.png"

export const Container = styled.div`
	background-color: var(--variant-gray);
	display: flex;
	flex-direction: column;
	color: var(--white);

	.colored_text {
		background: var(--purple-gradient);
		color: transparent;
		background-clip: text;
		-webkit-background-clip: text;
	}

	figure {
		background-color: var(--light-gray);
		position: relative;
		padding: 1rem;

		h2 {
			font-size: 2rem;

			span {
				margin-right: 0.5rem;
				color: var(--white);
				font-size: 1.2rem;
				cursor: pointer;

				svg {
					transform: rotateX(180deg);
					margin-bottom: 0.3rem;
				}
			}
		}

		div.img_container {
			width: 220px;
			text-align: center;
			margin: 1rem auto 0;
		}

		div.img_container img {
			height: 250px;
			width: fit-content;
		}

		div.img_container.default_img {
			min-height: 250px;
			background: url(${defaultBeer});
			background-size: 13.5rem;
			background-position-x: center;
			background-position-y: -25px;
			background-repeat: no-repeat;
			position: relative;

			img {
				position: absolute;
				top: 50%;
				left: 50%;
				transform: translate(-50%, -15%);
				width: 30%;
				height: min-content;
			}
		}

		p {
			position: absolute;
			transform: translateY(17vw);
			left: 0;
			bottom: 0;

			img {
				width: 100%;
				height: min-content;
			}
		}
	}

	div {
		z-index: 1;

		h3 {
			margin-left: 1rem;
			width: min-content;
			background: var(--yellow-gradient);
			color: transparent;
			background-clip: text;
			-webkit-background-clip: text;
		}

		p.beer__tagline {
			text-align: center;
			font-size: 0.8rem;
			padding: 0 1rem 1rem;
			font-weight: lighter;
		}

		p.beer__description {
			padding: 0.5rem 1rem 0;
			font-weight: lighter;
			text-align: justify;
			font-size: 0.9rem;
		}

		div.add_beer {
			padding: 2rem 1rem;

			p {
				text-align: center;
				margin-bottom: 0.5rem;
			}
			div {
				display: flex;
				justify-content: space-evenly;

				button {
					padding: 0.5rem 1rem;
					border-radius: 4rem;
					border: 0;
					transition: background-color 1s ease;
				}
			}
		}

		ul {
			padding: 0 1rem 1.5rem;

			li {
				padding: 0.5rem;
				font-size: 0.9rem;
			}

			li::before {
				content: attr(dataatrr);
				margin-right: 0.5rem;
			}

			li ul li {
				font-size: 0.82rem;
			}
		}
	}

	@media only screen and (min-width: 1100px) {
		flex-direction: row;
		min-height: calc(100vh - 88px);
		overflow: hidden;

		figure {
			width: 100%;
			max-width: 400px;

			div.img_container {
				width: 400px;
				height: 100%;
				margin: 0;

				img {
					height: 450px;
				}
			}

			div.img_container.default_img {
				width: 400px;
				background-size: 95%;

				img {
					width: 27%;
					transform: translate(-50%, -20%);
				}
			}

			p {
				transform: unset;
				height: 100%;
				right: -100%;
				width: 100%;
				left: initial;
				overflow: hidden;

				img {
					position: absolute;
					transform: rotate(-90deg);
					top: 40vh;
					left: calc(-100vh + 58vh);
					width: 100vh;
				}
			}
		}

		div {
			padding: 1.5rem 5rem 0;
			margin: 0 auto;
			max-width: 750px;

			ul li ul {
				display: flex;
				flex-wrap: wrap;
				gap: 0.5rem;
			}

			ul li ul li {
				padding: 0;
			}
		}
	}
`
