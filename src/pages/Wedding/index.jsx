import { useHistory } from "react-router-dom"
import styled from "styled-components"
import Beer from "../../components/Beer"
import { useWeeding } from "../../Providers/weeding"
import { Container } from "./style"

const item = {
	visible: (i) => ({
		opacity: 1,
		y: 0,
		transition: {
			delay: i * 0.2,
		},
	}),
	hidden: { opacity: 0, y: 30 },
}

const container = {
	visible: {
		opacity: 1,
		transition: {
			when: "beforeChildren",
			staggerChildren: 0.3,
		},
	},
	hidden: {
		opacity: 0,
		transition: {
			when: "afterChildren",
		},
	},
}

const Button = styled.button`
	margin-top: 0.5rem;
	padding: 0.5rem 1rem;
	background: var(--purple-gradient);
	border: 0;
	border-radius: 5rem;
`

const Weeding = () => {
	const { beers } = useWeeding()
	const history = useHistory()

	return (
		<Container variants={container} initial="hidden" animate="visible">
			<h2>Weeding</h2>
			{beers.length !== 0 ? (
				<div>
					{beers.map((beer, i) => (
						<Beer
							custom={i}
							initial="hidden"
							animate="visible"
							variants={item}
							key={beer.id}
							beer={beer}
							page="weeding"
						/>
					))}
				</div>
			) : (
				<>
					<p className="empty">Empty! No 🍺 here...</p>
					<div>
						<Button onClick={() => history.push("/")}>get some!</Button>
					</div>
				</>
			)}
		</Container>
	)
}

export default Weeding
