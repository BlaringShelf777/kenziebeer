import { motion } from "framer-motion"
import styled from "styled-components"

export const Container = styled(motion.div)`
	padding: 2rem 0.5rem;
	margin: 0 auto;
	max-width: 1200px;

	h2 {
		text-align: center;
		margin-bottom: 1rem;
		font-size: 2.5rem;
		font-weight: bold;
		text-transform: capitalize;
		background-image: var(--purple-gradient);
		color: transparent;
		background-clip: text;
		-webkit-background-clip: text;
	}

	p.empty {
		text-align: center;
		color: var(--white);
		font-weight: lighter;
	}

	& > div {
		display: flex;
		flex-wrap: wrap;
		gap: 0.5rem;
		justify-content: center;
		align-items: center;
	}

	@media only screen and (min-width: 750px) {
		div {
			gap: 1.2rem;
		}
	}
`
