import Navigation from "./components/Navigation"
import Routes from "./Routes"

function App() {
	return (
		<div>
			<Navigation />
			<Routes />
		</div>
	)
}

export default App
