import { useHistory } from "react-router-dom"
import { Container } from "./nav.style"
import { GiBalloons, GiBigDiamondRing, GiGraduateCap } from "react-icons/gi"
import { IoBeer } from "react-icons/io5"
import { desktopAnimation, mobileAnimation } from "./nav.animation"

const Navigation = () => {
	const history = useHistory()
	const handleNavigation = (path) => history.push(path)

	const handleAnimation = () => {
		if (window.screen.width >= 750) return desktopAnimation

		return mobileAnimation
	}

	return (
		<Container initial="hidden" animate="visible" variants={handleAnimation()}>
			<h1 onClick={() => handleNavigation("/")}>
				Get
				<IoBeer />
			</h1>

			<nav>
				<span className="nav__desktop">Events | </span>
				<ul className="nav__desktop">
					<li onClick={() => handleNavigation("/weeding")}>Weeding</li>
					<li onClick={() => handleNavigation("/getTogether")}>Get Together</li>
					<li onClick={() => handleNavigation("/graduation")}>Graduation</li>
				</ul>
				<ul className="nav__mobile">
					<li onClick={() => handleNavigation("/weeding")}>
						<GiBigDiamondRing />
					</li>
					<li onClick={() => handleNavigation("/getTogether")}>
						<GiBalloons />
					</li>
					<li onClick={() => handleNavigation("/graduation")}>
						<GiGraduateCap />
					</li>
				</ul>
			</nav>
		</Container>
	)
}

export default Navigation
