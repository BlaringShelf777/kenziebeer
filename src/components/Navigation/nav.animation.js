const hidden = {
	opacity: 0,
	y: 30,
	transition: {
		when: "afterChildren",
	},
}

const visible = {
	opacity: 1,
	y: 0,
	transition: {
		duration: 0.3,
	},
}

export const mobileAnimation = {
	visible,
	hidden,
}

export const desktopAnimation = {
	visible,
	hidden: {
		...hidden,
		y: -30,
	},
}
