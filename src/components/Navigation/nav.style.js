import { motion } from "framer-motion"
import styled from "styled-components"

export const Container = styled(motion.div)`
	display: flex;
	justify-content: space-between;
	align-items: center;
	width: 100%;
	background-color: var(--dark-gray);
	position: fixed;
	left: 0;
	bottom: 0;
	padding: 1rem 1.5rem 0.2rem;
	border-radius: 2rem 2rem 0 0;
	color: var(--white);
	z-index: 100;
	box-shadow: 0 0 0.5rem 0.2rem rgba(0, 0, 0, 0.5);

	h1 {
		display: flex;
		align-items: center;
		cursor: pointer;

		svg {
			margin-bottom: 0.5rem;
			color: #ffc107;
		}
	}

	nav {
		display: flex;
		align-items: center;

		ul {
			display: flex;
			gap: 1rem;
			align-items: center;
			justify-content: center;

			li {
				cursor: pointer;
			}
		}

		ul.nav__mobile {
			gap: 2rem;

			li {
				font-size: 1.5rem;
			}
		}

		ul.nav__desktop {
			display: none;
		}

		span.nav__desktop {
			display: none;
		}
	}

	& ~ div {
		margin-bottom: 3.9rem;
	}

	@media only screen and (min-width: 750px) {
		top: 0;
		bottom: auto;
		border-radius: 0;
		padding: 1rem;

		nav {
			span.nav__desktop {
				display: initial;
				font-weight: bold;
			}

			ul.nav__desktop {
				display: flex;
				padding: 1rem;

				li {
					span {
						background-image: var(--yellow-gradient);
						background-clip: text;
						-webkit-background-clip: text;
					}
				}
			}

			ul.nav__mobile {
				display: none;
			}
		}

		& ~ div {
			margin-bottom: 0;
			margin-top: 5.5rem;
		}
	}
`
