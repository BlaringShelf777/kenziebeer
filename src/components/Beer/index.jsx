import { Container } from "./beer.style"
import { GiBalloons, GiBigDiamondRing, GiGraduateCap } from "react-icons/gi"
import beerDecoration from "../../assets/beer_decoration.svg"
import { useWeeding } from "../../Providers/weeding"
import { useGetTogether } from "../../Providers/getTogether"
import { useGraduation } from "../../Providers/graduation"
import { useHistory } from "react-router-dom"

const Beer = ({ beer, catalogue = false, page, ...rest }) => {
	const history = useHistory()
	const { name, tagline, image_url } = beer
	const { addBeer: weedingAdd, removeBeer: weedingRemove } = useWeeding()
	const { addBeer: getTogetherAdd, removeBeer: getTogetherRemove } =
		useGetTogether()
	const { addBeer: graduationAdd, removeBeer: graduationRemove } =
		useGraduation()

	const parseString = (string, maxLen) => {
		if (string.length > maxLen) return string.slice(0, maxLen).trim() + "..."

		return string
	}

	const handleClick = ({ target }) => {
		if (
			target.classList.contains("buttons_container") ||
			target.closest("div.buttons_container")
		)
			return

		history.push(`/beer/${beer.id}`)
	}

	return (
		<Container {...rest} catalogue={catalogue} onClick={handleClick}>
			<div
				className={
					image_url.toLowerCase().endsWith("keg.png")
						? "img_container default_img"
						: "img_container"
				}
			>
				<img src={image_url} alt={name} />
			</div>
			<p className="beer__decoration">
				<img src={beerDecoration} alt="decoration" />
			</p>
			<h3>{parseString(name, 13)}</h3>
			<p>{parseString(tagline, 40)}</p>
			<div className="buttons_container">
				<div>{catalogue ? "+" : "-"}</div>
				<ul>
					{catalogue ? (
						<>
							<li onClick={() => weedingAdd(beer)}>
								<GiBigDiamondRing />
							</li>
							<li onClick={() => getTogetherAdd(beer)}>
								<GiBalloons />
							</li>
							<li onClick={() => graduationAdd(beer)}>
								<GiGraduateCap />
							</li>
						</>
					) : (
						<li
							className="remove_beer"
							onClick={() =>
								page === "weeding"
									? weedingRemove(beer)
									: page === "graduation"
									? graduationRemove(beer)
									: getTogetherRemove(beer)
							}
						>
							remove
						</li>
					)}
				</ul>
			</div>
		</Container>
	)
}

export default Beer
