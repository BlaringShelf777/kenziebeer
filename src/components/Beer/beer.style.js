import { motion } from "framer-motion"
import styled, { css } from "styled-components"
import defaultBeer from "../../assets/defaultBeer.png"

export const Container = styled(motion.div)`
	display: flex;
	position: relative;
	width: 165px;
	flex-direction: column;
	align-items: space-between;
	background-color: var(--variant-gray);
	padding: 0.5rem;
	overflow-x: hidden;
	border-radius: 10px;
	box-shadow: 0 0 0.5rem 0.2rem rgba(0, 0, 0, 0.5);
	cursor: pointer;

	* {
		z-index: 2;
	}

	img {
		height: 250px;
		width: fit-content;
		margin: 0 auto;
	}

	div.img_container.default_img {
		min-height: 250px;
		background: url(${defaultBeer});
		background-size: 13.5rem;
		background-position-x: center;
		background-position-y: -25px;
		background-repeat: no-repeat;
		position: relative;

		img {
			position: absolute;
			top: 50%;
			left: 50%;
			transform: translate(-50%, -15%);
			width: 38%;
			height: auto;
		}
	}

	p.beer__decoration {
		position: absolute;
		background-color: var(--light-gray);
		height: 250px;
		top: 0;
		left: -0.5rem;
		z-index: 0;

		img {
			position: relative;
			top: 248px;
			width: 100%;
			height: min-content;
		}
	}

	h3 {
		color: var(--white);
		margin-top: 1rem;
	}

	p:not(.beer__decoration) {
		font-size: 0.8rem;
		height: 40px;
		background-image: var(--purple-gradient);
		color: transparent;
		background-clip: text;
		-webkit-background-clip: text;
		margin-bottom: 1rem;
	}

	div {
		display: flex;
		align-items: center;
		justify-content: space-between;
	}

	div:not(.img_container) {
		gap: 0.5rem;
		width: calc(var(--add-size) + 2px);
		height: calc(var(--add-size) + 2px);
		overflow: hidden;
		transition: all 0.3s ease;
		border-radius: 5rem;
		background: var(--purple-gradient);
		background: ${(props) =>
			props.catalogue
				? css`var(--cyan-gradient)`
				: css`var(--yellow-gradient)`};

		&:hover {
			width: 100%;

			& div {
				display: grid;
				place-items: center;
			}
		}

		div {
			margin-left: 1px;
			background: var(--light-gray);
			color: var(--white);
			max-width: var(--add-size);
			max-height: var(--add-size);
			font-weight: bold;
		}

		div,
		ul,
		ul li {
			display: flex;
			justify-content: center;
			align-items: center;
		}

		div,
		ul li {
			flex: 1;
			min-height: var(--add-size);
			min-width: var(--add-size);
			border-radius: 50%;
		}

		ul {
			gap: 0.5rem;
			margin-right: 1px;

			li {
				background: var(--variant-gray);
				color: white;
			}
		}

		ul li.remove_beer {
			border-radius: 5rem;
			padding: 0 1.5rem;
			font-weight: lighter;
		}
	}

	@media only screen and (min-width: 750px) {
		width: 220px;
		padding: 1.5rem;
	}
`
