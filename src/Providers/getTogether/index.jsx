import { createContext, useContext, useEffect, useState } from "react"
import { toast } from "react-toastify"

const GetTogetherContext = createContext()

export const GetTogetherProvider = ({ children }) => {
	const [beers, setBeers] = useState([])

	const addBeer = (beer) => {
		if (beers.find((b) => b.id === beer.id))
			return toast.error("🍺 Already added!")

		setBeers([...beers, beer])

		return toast.success("🍺 Added!")
	}

	const removeBeer = (beer) => {
		const new_beers = beers.filter((b) => b.id !== beer.id)

		if (new_beers.length === beers.length) return

		setBeers(new_beers)

		return toast.success("🍺 Removed!")
	}

	useEffect(() => {
		const initialState = JSON.parse(
			localStorage.getItem("@getbeer/getTogether")
		)

		if (initialState) {
			setBeers(initialState)
		} else {
			setBeers([])
		}
	}, [])

	useEffect(() => {
		localStorage.setItem("@getbeer/getTogether", JSON.stringify(beers))
	}, [beers])

	return (
		<GetTogetherContext.Provider value={{ beers, addBeer, removeBeer }}>
			{children}
		</GetTogetherContext.Provider>
	)
}

export default GetTogetherProvider

export const useGetTogether = () => useContext(GetTogetherContext)
