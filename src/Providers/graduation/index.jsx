import { createContext, useContext, useEffect, useState } from "react"
import { toast } from "react-toastify"

const GraduationContext = createContext()

export const GraduationProvider = ({ children }) => {
	const [beers, setBeers] = useState([])

	const addBeer = (beer) => {
		if (beers.find((b) => b.id === beer.id))
			return toast.error("🍺 Already added!")

		setBeers([...beers, beer])

		return toast.success("🍺 Added!")
	}

	const removeBeer = (beer) => {
		const new_beers = beers.filter((b) => b.id !== beer.id)

		if (new_beers.length === beers.length) return

		setBeers(new_beers)

		return toast.success("🍺 Removed!")
	}

	useEffect(() => {
		const initialState = JSON.parse(localStorage.getItem("@getbeer/graduation"))

		if (initialState) {
			setBeers(initialState)
		} else {
			setBeers([])
		}
	}, [])

	useEffect(() => {
		localStorage.setItem("@getbeer/graduation", JSON.stringify(beers))
	}, [beers])

	return (
		<GraduationContext.Provider value={{ beers, addBeer, removeBeer }}>
			{children}
		</GraduationContext.Provider>
	)
}

export const useGraduation = () => useContext(GraduationContext)
