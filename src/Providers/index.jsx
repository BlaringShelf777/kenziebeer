import { GetTogetherProvider } from "./getTogether"
import { GraduationProvider } from "./graduation"
import { WeedingProvider } from "./weeding"

const Providers = ({ children }) => {
	return (
		<WeedingProvider>
			<GetTogetherProvider>
				<GraduationProvider>{children}</GraduationProvider>
			</GetTogetherProvider>
		</WeedingProvider>
	)
}

export default Providers
