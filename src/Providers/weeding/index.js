import { createContext, useContext, useEffect, useState } from "react"
import { toast } from "react-toastify"

const WeedingContext = createContext()

export const WeedingProvider = ({ children }) => {
	const [beers, setBeers] = useState([])

	const addBeer = (beer) => {
		if (beers.find((b) => b.id === beer.id))
			return toast.error("🍺 Already added!")

		setBeers([...beers, beer])

		return toast.success("🍺 Added!")
	}

	const removeBeer = (beer) => {
		const new_beers = beers.filter((b) => b.id !== beer.id)

		if (new_beers.length === beers.length) return

		setBeers(new_beers)

		return toast.success("🍺 Removed!")
	}

	useEffect(() => {
		const initialState = JSON.parse(localStorage.getItem("@getbeer/weeding"))

		if (initialState) {
			setBeers(initialState)
		} else {
			setBeers([])
		}
	}, [])

	useEffect(() => {
		localStorage.setItem("@getbeer/weeding", JSON.stringify(beers))
	}, [beers])

	return (
		<WeedingContext.Provider value={{ beers, addBeer, removeBeer }}>
			{children}
		</WeedingContext.Provider>
	)
}

export const useWeeding = () => useContext(WeedingContext)
