import { createGlobalStyle } from "styled-components"

export default createGlobalStyle`
    :root {
            --add-size: 30px;
            --white: #f8f8f2;
            --light-gray: #22212c;
            --mid-gray: #1d1e26;
            --dark-gray: #14141b;
            --variant-gray: #161b22;
            --cyan-gradient: linear-gradient(45deg, #80ffea 0, #8aff80 100%);
            --purple-gradient: linear-gradient(45deg, #9580ff 0, #80ffea 100%);
            --yellow-gradient: linear-gradient(45deg, #ffff80 0, #ff80bf 100%);
        }
    * {
        padding: 0;
        margin: 0;
        list-style: none;
        box-sizing: border-box;
        font-family: 'Nunito', sans-serif;
        -webkit-tap-highlight-color: transparent;
    }

    body {
        background-color: var(--mid-gray);
    }

    button {
        cursor: pointer;
    }
`
