import React from "react"
import ReactDOM from "react-dom"
import App from "./App"
import { BrowserRouter as Router } from "react-router-dom"
import { ToastContainer } from "react-toastify"
import "react-toastify/dist/ReactToastify.css"
import GlobalStyle from "./style/global"
import Providers from "./Providers"
import { AnimatePresence } from "framer-motion"

ReactDOM.render(
	<React.StrictMode>
		<Providers>
			<AnimatePresence>
				<Router>
					<App />
					<GlobalStyle />
					<ToastContainer />
				</Router>
			</AnimatePresence>
		</Providers>
	</React.StrictMode>,
	document.getElementById("root")
)
