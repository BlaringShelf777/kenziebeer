import { Route, Switch } from "react-router-dom"
import Catalogue from "../pages/Catalogue"
import GetTogether from "../pages/GetTogether"
import Weeding from "../pages/Wedding"
import Graduation from "../pages/Graduation"
import BeerPage from "../pages/BeerPage"

const Routes = () => (
	<Switch>
		<Route exact path="/">
			<Catalogue />
		</Route>
		<Route path="/weeding">
			<Weeding />
		</Route>
		<Route path="/getTogether">
			<GetTogether />
		</Route>
		<Route path="/graduation">
			<Graduation />
		</Route>
		<Route path="/beer/:id">
			<BeerPage />
		</Route>
	</Switch>
)

export default Routes
